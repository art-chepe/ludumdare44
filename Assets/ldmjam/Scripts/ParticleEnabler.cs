﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEnabler : MonoBehaviour
{
    ParticleSystem[] particles;
    // Start is called before the first frame update
    private void Start()
    {
        particles = GetComponentsInChildren<ParticleSystem>();
        EnableParticles(false);
    }

    public void EnableParticles (bool active)
    {
        foreach(ParticleSystem system in particles)
        {
            ParticleSystem.EmissionModule module = system.emission;
            module.enabled = active;
        }
    }
}
