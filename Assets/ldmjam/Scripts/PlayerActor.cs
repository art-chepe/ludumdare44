﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public enum MovementDirection { Horizontal, Vertical }

public class PlayerActor : MonoBehaviour
{
    public int playerIndex;
    public Transform playerTransform;
    public MovementDirection direction;
    public bool inverseMovement;
    public float limitation = 3;
    public float speed = 8;
    public float speedBoost = 16;
    public float currSpeed;
    public Animator anim;
    public GameObject electricity;
    
    private Player player;
    private Vector3 movement;
    private float x, z;
    public bool dead;
    public AudioSource moveSource;

    private BoxCollider col;

    private void Start ()
    {
        player = ReInput.players.GetPlayer(playerIndex);
        dead = true;
    }

    public void Dead (bool isDead)
    {
        anim.SetBool("Dead", isDead);
        dead = isDead;
        electricity.SetActive(isDead);
    }
    
    public void DeadOver (bool isDead)
    {
        anim.SetBool("DeadOver", isDead);
        dead = isDead;
        electricity.SetActive(isDead);
    }

    public IEnumerator ShootAudio ()
    {
        yield return new WaitForSeconds(0.22f);
        moveSource.PlayOneShot(moveSource.clip);
    }

    private void Update ()
    {
        if(player.GetButtonDown("Horizontal") || player.GetNegativeButtonDown("Horizontal"))
        {
            StartCoroutine(ShootAudio());
        }   

        if(!dead) 
        {
            x = player.GetAxis("Horizontal");
            z = player.GetAxis("Vertical");
        }
        else 
        {
            x = 0; z = 0;
        }

        anim.SetFloat("Direction", inverseMovement ? x : -x);

        if(direction == MovementDirection.Horizontal)
        {
            currSpeed = inverseMovement ? -x : x;
            movement.x = Mathf.Clamp(movement.x, -limitation, limitation);
        }
        else
        {
            currSpeed = inverseMovement ? -z : z;
            movement.z = Mathf.Clamp(movement.z, -limitation, limitation);
        }

        currSpeed *= Time.deltaTime * (player.GetButton("Boost") ? speedBoost : speed);

        playerTransform.Translate(currSpeed, 0, 0);

        movement = playerTransform.position;

        if(direction == MovementDirection.Horizontal)
        {
            movement.x = Mathf.Clamp(movement.x, -limitation, limitation);
        }
        else
        {
            movement.z = Mathf.Clamp(movement.z, -limitation, limitation);
        }

        playerTransform.position = movement;
    }

    public BoxCollider GetCollider ()
    {
        if(col == null) col = GetComponent<BoxCollider>();
        return col;
    }
}
