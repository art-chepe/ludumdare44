﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Goals : MonoBehaviour
{
    public int playerIndex;
    public PlayerActor player;
    public Image face;
    public Text moneyLabel;
    public Text lifesLabel;

    public bool debugMoney;
    public int money = 500;
    public int lifes = 3;

    public BallBehaviour ball;
    public GameLoop manager;

    private void Start()
    {
        ball = FindObjectOfType<BallBehaviour>();
        lifes = 3;
        money = 500;
        lifesLabel.text = lifes.ToString();
        moneyLabel.text = money.ToString();
    }

    private void OnTriggerEnter (Collider other)
    {
        if(other.gameObject.name.Contains("frag")) return;

        lifes--;
        lifesLabel.text = lifes.ToString();
        if(lifes <= 0)
        {
            lifes = 3;
            money -= debugMoney ? 500 : 125;
            lifesLabel.text = lifes.ToString();
            moneyLabel.text = money.ToString();
            manager.AddMoney(playerIndex == 0 ? 0 : 1, debugMoney ? 500 : 125);
        }

        if(money > 0) 
        {
            face.sprite = manager.faces[lifes-1];
            StartCoroutine(ResetBall());
        }
        else 
        {
            player.DeadOver(true);
            StartCoroutine(SetGameOver());
        }
    }

    IEnumerator SetGameOver()
    {
        manager.CloseDoor(playerIndex);
        yield return new WaitForSeconds(manager.secondsBeforeEnding + 1);
        manager.GameOver(playerIndex);
    }

    IEnumerator ResetBall ()
    {
        player.Dead(true);
        yield return new WaitForSeconds(ball.secondsAfterEachGoal);
        ball.ResetBall();
        player.Dead(false);
    }

    public void AddMoney (int amount)
    {
        money += amount;
        moneyLabel.text = money.ToString();
    }
}
