﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehaviour : MonoBehaviour
{
    public float speed = 1;
    public float speedAddition = 0.1f;
    public float secondsAfterEachGoal = 2;
    public Vector3 movement;
    public AudioSource ballSource;

    private float extraSpeed;
    private bool roll;

    public void StartGame ()
    {        
        StartCoroutine(WaitCoupleSeconds());
    }

    // Update is called once per frame
    private void Update ()
    {
        if(roll)
            transform.Translate(movement * (speed + extraSpeed) * Time.deltaTime);
    }

    private void OnCollisionEnter (Collision other)
    {
        PlayerActor actor = other.gameObject.GetComponent<PlayerActor>();

        ballSource.PlayOneShot(ballSource.clip);

        if(actor != null)  
        {
            if(actor.direction == MovementDirection.Horizontal)
            {
                movement.z *= -1;
                movement.x = HitFactor(transform.position, 
                actor.playerTransform.position, 
                actor.GetCollider().bounds.size.x).normalized.x;
            }
            else
            {
                movement.x *= -1;
                movement.z = HitFactor(transform.position, 
                actor.playerTransform.position, 
                actor.GetCollider().bounds.size.x).normalized.z;
            }
            extraSpeed += speedAddition;
        }
        else 
        {
            if(other.gameObject.CompareTag("Horizontal"))
            {
                movement.z *= -1;
            }
            else if(other.gameObject.CompareTag("Vertical"))
            {
                movement.x *= -1;
            }
            else if(other.gameObject.CompareTag("Both"))
            {
                movement.x *= -1;
                movement.z *= -1;
            }
        }
    }

    public void ResetBall ()
    {
        extraSpeed = 0;
        movement = Vector3.zero;
        transform.position = Vector3.up * 0.36f;
        roll = false;
        StartCoroutine(WaitCoupleSeconds());
    }

    IEnumerator WaitCoupleSeconds ()
    {  
        movement.x = Random.Range(-0.5f, 0.5f);
        movement.z = Random.Range(-1, 1);

        yield return new WaitForSeconds(secondsAfterEachGoal);   

        if(movement.z == 0)
        {
            movement.z = 1;
        }

        roll = true;
    }

    private Vector3 HitFactor (Vector3 ballPos, Vector3 paddlePos, float paddleHeight)
    {
        return new Vector3((ballPos.x - paddlePos.x) / paddleHeight, 0, (ballPos.z - paddlePos.z) / paddleHeight);
    }

     
}
