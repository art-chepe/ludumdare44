﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameLoop : MonoBehaviour
{
    public bool gameover;
    public int secondsBeforeEnding = 4;
    public Goals[] players;
    public Animator[] doors;
    public Sprite[] faces;
    public Text label1, label2;
    public Image face1, face2;
    public Sprite winner, loser;
    public GameObject gameoverWindow;

    public void Restart ()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void Update ()
    {
        if(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.LeftCommand))
        {
            if(Input.GetKeyDown(KeyCode.R))
            {
                Restart();
            }
        }
    }

    public void CloseDoor (int playerIndex)
    {
        doors[playerIndex].enabled = true;
    }

    public void GameOver (int playerIndex)
    {        
        // Get the winner
        int p = playerIndex + 1;
        if(p > 1) p = 0;

        label1.text =  string.Format(label1.text, (p+1).ToString());
        label2.text =  string.Format(label2.text, (p+1).ToString());

        if(p == 0) 
        {
            face1.sprite = winner;
            face2.sprite = loser;
        }
        else
        {
            face1.sprite = loser;
            face2.sprite = winner;
        }

        gameoverWindow.SetActive(true);
    }

    public void AddMoney (int playerIndex, int amount)
    {
        players[playerIndex].AddMoney(amount);
    }
}
